# JAX-RS Japanese Holidays Application

Appleの提供しているJapanese32holidays.icsを解析して、指定年月の祝日を取得する為のサービスです。
heroku上でサービスを公開中です。  

以下のように指定する事により祝日を取得することができます。（この例では2012年7月のイベントを取得します）
```
http://japanese-holidays.herokuapp.com/services/ical/2012/7
```

## Running the application locally

First build with:

    $ mvn clean install

Then run it with:

    $ java -cp target/classes:target/dependency/* com.example.Main

