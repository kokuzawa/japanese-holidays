/*
 * Copyright 2012 Katsumi Kokuzawa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.co.baykraft.services;

import java.util.*;
import jp.co.baykraft.models.Holiday;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * カレンダーサービステストクラスです。
 * @author Katsumi
 */
public class CalServiceTest
{

    public CalServiceTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    private Date getEventDate(int year, int month, int date)
    {
        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("Asia/Tokyo"));
        cal.clear();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DATE, date);
        return cal.getTime();
    }

    @Test
    public void testGetHolidayByMonth_201201() throws Exception
    {
        final Integer year = 2012;
        final Integer month = 1;
        final CalService instance = new CalService();
        final Collection<Holiday> expResult = new ArrayList<Holiday>();
        expResult.add(new Holiday("元日 (New Year's Day)", getEventDate(2012, 0, 1)));
        expResult.add(new Holiday("振替休日 (Substitute Holiday)", getEventDate(2012, 0, 2)));
        expResult.add(new Holiday("成人の日 (Coming of Age Day)", getEventDate(2012, 0, 9)));

        Collection result = instance.getHolidayByMonth(year, month);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetHolidayByMonth_201202() throws Exception
    {
        final Integer year = 2012;
        final Integer month = 2;
        final CalService instance = new CalService();
        final Collection<Holiday> expResult = new ArrayList<Holiday>();
        expResult.add(new Holiday("建国記念の日 (National Foundation Day)", getEventDate(2012, 1, 11)));

        Collection result = instance.getHolidayByMonth(year, month);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetHolidayByMonth_201207() throws Exception
    {
        final Integer year = 2012;
        final Integer month = 7;
        final CalService instance = new CalService();
        final Collection<Holiday> expResult = new ArrayList<Holiday>();
        expResult.add(new Holiday("海の日 (Marine Day)", getEventDate(2012, 6, 16)));

        Collection result = instance.getHolidayByMonth(year, month);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetHolidayByMonth_201209() throws Exception
    {
        final Integer year = 2012;
        final Integer month = 9;
        final CalService instance = new CalService();
        final Collection<Holiday> expResult = new ArrayList<Holiday>();
        expResult.add(new Holiday("敬老の日 (Respect for the Aged Day)", getEventDate(2012, 8, 17)));
        expResult.add(new Holiday("秋分の日 (Autumnal Equinox)", getEventDate(2012, 8, 22)));

        Collection result = instance.getHolidayByMonth(year, month);
        assertEquals(expResult, result);
    }
}
