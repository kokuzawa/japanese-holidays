/*
 * Copyright 2012 Katsumi Kokuzawa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.co.baykraft.services;

import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.*;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import jp.co.baykraft.models.Holiday;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.CalendarParserFactory;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ParameterFactoryRegistry;
import net.fortuna.ical4j.model.ParameterList;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.PropertyFactory;
import net.fortuna.ical4j.model.PropertyFactoryRegistry;
import net.fortuna.ical4j.model.Recur;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.WeekDay;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.RRule;
import net.fortuna.ical4j.model.property.Summary;

/**
 * Appleの提供する
 * <a href="http://ical.mac.com/ical/Japanese32Holidays.ics">http://ical.mac.com/ical/Japanese32Holidays.ics</a>
 * を解析し、指定された年月の日本の祝日イベントを取得するためのサービスです。
 * 現在サポートしているのは、/ical/{year}/{month} 形式のURLを指定する事により、
 * 指定月の祝日イベントを取得するインタフェースになります。
 * @author Katsumi
 */
@Path("/ical")
@Produces(MediaType.APPLICATION_JSON)
public class CalService
{
    /**
     * 日本の祝日ICSのURL
     */
    private static final String ICS_URL = "http://ical.mac.com/ical/Japanese32Holidays.ics";

    /**
     * "PREFERRED_LANGUAGE"プロパティ
     */
    private static final String PREFERRED_LANGUAGE = "PREFERRED_LANGUAGE";

    /**
     * /ical/{year}/{month} 形式のパスにより、指定年月の祝日イベントを取得します。
     * イベントは日付の昇順で取得できます。
     * @param year 年
     * @param month 月
     * @return 祝日イベントのリスト
     * @throws IOException Japanese32Holidays.ics の読み込みに失敗した場合
     * @throws ParserException Japanese32Holidays.ics の解析に失敗した場合
     */
    @GET
    @Path("/{year}/{month}")
    public Collection<Holiday> getHolidayByMonth(@PathParam("year") Integer year, @PathParam("month") Integer month)
            throws IOException, ParserException
    {
        final List<Holiday> events = new ArrayList<Holiday>();

        for (Object object : getCalendar(ICS_URL).getComponents(Component.VEVENT)) {
            Component component = (Component) object;
            RRule rRule = (RRule) component.getProperty(Property.RRULE);
            DtStart dtStart = (DtStart) component.getProperty(Property.DTSTART);
            DtEnd dtEnd = (DtEnd) component.getProperty(Property.DTEND);
            Summary summary = (Summary) component.getProperty(Property.SUMMARY);
            // 指定年月のイベント以外も含まれるため、ここで対象イベントかどうか判定する。
            if (isSkipEvent(rRule, dtStart, dtEnd, year, month)) {
                continue;
            }
            // RRULEが無い場合はDTSTARTをイベントの日付とする。
            if (null == rRule) {
                java.util.Date eventDate = getHolidayEventDate(null, dtStart, year, month);
                events.add(new Holiday(summary.getValue(), eventDate));
            }
            // RRULEが指定されている場合はRRULEから日付を算出する。
            // 但し、本処理では月が指定されているため、指定月以外のイベントはスキップする。
            else if (isTargetMonth(rRule.getRecur(), month)) {
                java.util.Date eventDate = getHolidayEventDate(rRule, dtStart, year, month);
                events.add(new Holiday(summary.getValue(), eventDate));
            }
        }
        // 日付の昇順に並び替える。
        Collections.sort(events, new EventSorter());
        return events;
    }

    /**
     * ICSファイルを解析した結果を取得します。
     * @param icsUrl ICSファイルURL
     * @return 解析結果
     * @throws IOException Japanese32Holidays.ics の読み込みに失敗した場合
     * @throws ParserException Japanese32Holidays.ics の解析に失敗した場合
     */
    private Calendar getCalendar(String icsUrl) throws IOException, ParserException
    {
        final PropertyFactoryRegistry propertyFactoryRegistry = new PropertyFactoryRegistry();
        propertyFactoryRegistry.register(PREFERRED_LANGUAGE, new PreferredLanguageFactory());
        URLConnection con = new URL(icsUrl).openConnection();
        CalendarBuilder builder = new CalendarBuilder(
                CalendarParserFactory.getInstance().createParser(),
                propertyFactoryRegistry,
                new ParameterFactoryRegistry(),
                TimeZoneRegistryFactory.getInstance().createRegistry());
        return builder.build(con.getInputStream());
    }

    /**
     * イベントを日付の昇順でソートするためのクラスです。
     */
    private static class EventSorter implements Comparator<Holiday>, Serializable
    {
        private static final long serialVersionUID = 1L;

        @Override
        public int compare(Holiday t, Holiday t1)
        {
            return t.getDate().compareTo(t1.getDate());
        }
    }

    /**
     * ical4j-1.0.4ではサポートされていない"PREFERRED_LANGUAGE"プロパティを解釈するためのファクトリクラスです。
     * ical4jでサポートされた場合は不要になるクラスです。
     */
    private static class PreferredLanguageFactory implements PropertyFactory
    {
        private static final long serialVersionUID = 1L;

        @Override
        public Property createProperty(String name)
        {
            return new PreferredLanguageProperty();
        }

        @Override
        public Property createProperty(String name, ParameterList parameters, String value)
                throws IOException, URISyntaxException, ParseException
        {
            return new PreferredLanguageProperty();
        }
    }

    /**
     * ical4j-1.0.4ではサポートされていない"PREFERRED_LANGUAGE"プロパティを表すクラスです。
     * ical4jでサポートされた場合は不要になるクラスです。
     */
    private static class PreferredLanguageProperty extends Property
    {
        private static final long serialVersionUID = 1L;
        private String value;

        public PreferredLanguageProperty()
        {
            super(PREFERRED_LANGUAGE, new PreferredLanguageFactory());
        }

        @Override
        public void setValue(String aValue) throws IOException, URISyntaxException, ParseException
        {
            this.value = aValue;
        }

        @Override
        public void validate() throws ValidationException
        {
        }

        @Override
        public String getValue()
        {
            return value;
        }
    }

    /**
     * イベントが指定月のイベントかどうか判定します。
     * @param recur イベントの繰り返し情報
     * @param month 月
     * @return true: 指定月のイベント, false: 指定月のイベントではない
     */
    private boolean isTargetMonth(Recur recur, Integer month)
    {
        for (Object o : recur.getMonthList()) {
            Integer targetMonth = (Integer) o;
            if (targetMonth.equals(month)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 祝日は年1回だけなので本メソッド内ではルールの指定がある場合は、特定の1日だけを指していることとして処理します。
     * ルールの指定が無い場合は、year, month, TDSTARTで指定された日を元にイベント日時を取得します。
     * 祝日に時間は関係ないので、時間については無視します。
     * @param rRule RRULEプロパティ
     * @param dtStart DTSTARTプロパティ
     * @param year 年
     * @param month 月
     * @return イベントの日付
     */
    private java.util.Date getHolidayEventDate(RRule rRule, DtStart dtStart, Integer year, Integer month)
    {
        // 月は0スタート（1月が0）なので、指定された月をマイナスしておく。
        Integer currentMonth = month - 1;

        // 取得した情報からイベントの日付を生成する。
        GregorianCalendar cal = new GregorianCalendar();

        // タイムゾーンが指定されてない場合は終日イベントのため、時間をクリアする。
        if (null == dtStart.getTimeZone()) {
            Date date = new Date(dtStart.getDate().getTime() - TimeZone.getDefault().getRawOffset());
            cal.setTime(date);
        }
        else {
            cal.setTime(dtStart.getDate());
        }
        cal.set(GregorianCalendar.YEAR, year);
        cal.set(GregorianCalendar.MONTH, currentMonth);

        if (null == rRule) {
            return cal.getTime();
        }

        // WeekDay（第何週の何曜日）を取得する。
        WeekDay weekDay = null;
        if (!rRule.getRecur().getDayList().isEmpty()) {
            weekDay = (WeekDay) rRule.getRecur().getDayList().get(0);
        }
        if (null != weekDay) {
            cal.set(GregorianCalendar.DAY_OF_WEEK, WeekDay.getCalendarDay(weekDay));
            cal.set(GregorianCalendar.DAY_OF_WEEK_IN_MONTH, weekDay.getOffset());
        }

        return cal.getTime();
    }

    /**
     * year, monthで指定された年月のイベントかどうかを判定します。
     * 指定年月のイベント以外の場合はtrue, 指定年月のイベントの場合はfalseを返します。
     * @param rRule RRULEプロパティ
     * @param dtStart DTSTARTプロパティ
     * @param dtEnd DTENDプロパティ
     * @param year 年
     * @param month 月
     * @return true: 指定年月のイベントではない, false: 指定年月のイベントである
     */
    private boolean isSkipEvent(RRule rRule, DtStart dtStart, DtEnd dtEnd, Integer year, Integer month)
    {
        // 月は0スタート（1月が0）なので、指定された月をマイナスしておく。
        Integer currentMonth = month - 1;
        // DTSTARTはrfc2445を読む限り必ず指定されているはずだが、
        // 仕様に沿っていないファイルを読み込む可能性を考慮して、null判定を行う。
        // DTSTARTが指定年月に達していないイベントはスキップする。
        if (null != dtStart) {
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(dtStart.getDate());
            if (cal.get(GregorianCalendar.YEAR) > year) {
                return true;
            }
            else if (cal.get(GregorianCalendar.YEAR) == year && cal.get(GregorianCalendar.MONTH) > currentMonth) {
                return true;
            }
        }
        // RRULEの指定が無く、DTENDが指定されている場合において、
        // 指定年月がDTENDを超えているイベントはスキップする。
        if (null == rRule && null != dtEnd) {
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(dtEnd.getDate());
            if (cal.get(GregorianCalendar.YEAR) < year) {
                return true;
            }
            else if (cal.get(GregorianCalendar.YEAR) == year && cal.get(GregorianCalendar.MONTH) < currentMonth) {
                return true;
            }
        }

        return false;
    }
}
