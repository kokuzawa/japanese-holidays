/*
 * Copyright 2012 Katsumi Kokuzawa.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.co.baykraft.models;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * 祝日イベントモデルです。
 * @author Katsumi
 */
@XmlRootElement
public class Holiday
{
    /**
     * サマリー
     */
    private String summary;

    /**
     * 日時
     */
    private final Date date;

    /**
     * サマリー、日時を指定してインスタンスを生成します。
     * @param summary サマリー
     * @param date 日時
     */
    public Holiday(String summary, Date date)
    {
        this.summary = summary;
        this.date = null != date ? new Date(date.getTime()) : null;
    }

    /**
     * 日時を取得します。
     * @return 日時
     */
    public Date getDate()
    {
        return null != date ? new Date(date.getTime()) : null;
    }

    /**
     * サマリーを取得します。
     * @return サマリー
     */
    public String getSummary()
    {
        return summary;
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (null == o) {
            return false;
        }
        if (o.getClass() != this.getClass()) {
            return false;
        }
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this);
    }
}
